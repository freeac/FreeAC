/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */

#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include "xyz_log.h"
#include "wmp.h"

/*
 *
 */
int wmmInit(WMMSG *w, uint8_t ver, uint8_t type, uint8_t *apid, uint16_t seqnum, uint16_t code)
{
	if (!w || !apid) {
		xyz_log_error("Args error");
		return -1;
	}
	memset(w, 0, sizeof(WMMSG));
	w->buff[WMM_POS_VER] = ver;
	w->buff[WMM_POS_TYPE] = type;
	memcpy(&(w->buff[WMM_POS_APID]), apid, WMM_APID_SIZE);
	w->buff[WMM_POS_SEQNUM] = seqnum >> 8;
	w->buff[WMM_POS_SEQNUM + 1] = seqnum;
	w->buff[WMM_POS_CODE] = code >> 8;
	w->buff[WMM_POS_CODE + 1] = code;
	w->offset = WMM_POS_PAYLOAD;
	return 0;
}

/*
 *
 */
int wmmPutRawAttr(WMMSG *w, uint16_t type, void *value, uint16_t len)
{
	if (len > BUFF_SIZE) {
		xyz_log_error("Attribute is too long");
		return -1;
	}
	if (w->offset + 4 + len > WMM_BUFF_SIZE) {
		xyz_log_error("Maximum message length exceeded");
		return -1;
	}
	w->buff[w->offset++] = type >> 8;
	w->buff[w->offset++] = type;
	w->buff[w->offset++] = len >> 8;
	w->buff[w->offset++] = len;
	memcpy(&w->buff[w->offset], value, len);
	w->offset += len;
	w->payloadlen += (len + 4);
	w->buff[WMM_POS_PAYLEN] = w->payloadlen >> 8;
	w->buff[WMM_POS_PAYLEN + 1] = w->payloadlen;
	return 0;
}

int wmmGetAttr(WMMSG *w, BUFF *b)
{
	if (!w || !b) {
		xyz_log_error("Args error");
		return -1;
	}

	if (w->offset == w->totlen || w->offset > w->totlen) {
		return 0;
	}
	if (w->offset + 4 > w->totlen) {
		xyz_log_error("Malformed attribute in response");
		return -1;
	}
	b->type = w->buff[w->offset] << 8 | w->buff[w->offset + 1];
	w->offset += 2;
	b->len = w->buff[w->offset] << 8 | w->buff[w->offset + 1];
	w->offset += 2;
	if (w->offset + b->len > w->totlen) {
		xyz_log_error("Malformed attribute in response");
		return -1;
	}
	if (b->len > BUFF_SIZE) {
		xyz_log_error("Attribute value is too long %d > %d", b->len, BUFF_SIZE);
		return -1;
	}
	memcpy(b->buff, &(w->buff[w->offset]), b->len);
	w->offset += b->len;
	b->offset = 0;
	return 1;
}

int wmmPrepRecvd(WMMSG *w)
{
	if (!w) {
		xyz_log_error("Args error");
		return -1;
	}
	if (w->totlen < WMM_POS_PAYLOAD) {
		xyz_log_error("Message is too short");
		return -1;
	}
	w->payloadlen = w->buff[WMM_POS_PAYLEN] << 8 | w->buff[WMM_POS_PAYLEN + 1];
	if (w->totlen < w->payloadlen) {
		xyz_log_error("The actual length is less than the given length");
		return -1;
	}
	w->offset = WMM_POS_PAYLOAD;
	w->version = w->buff[WMM_POS_VER];
	w->type = w->buff[WMM_POS_TYPE];
	memcpy(w->apid, &(w->buff[WMM_POS_APID]), WMM_APID_SIZE);
	w->seqnum = w->buff[WMM_POS_SEQNUM] << 8 | w->buff[WMM_POS_SEQNUM + 1];
	w->code = w->buff[WMM_POS_CODE] << 8 | w->buff[WMM_POS_CODE + 1];

	return 0;
}
