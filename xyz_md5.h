/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */

#ifndef __XYZ_MD5_H__
#define __XYZ_MD5_H__

#include "xyz_int.h"

#define XYZ_MD5_DIGEST_LENGTH 16


typedef struct {
  xyz_u32_t buf[4];
  xyz_u32_t bits[2];
  xyz_u8_t in[64];
} xyz_md5ctx_t;

void xyz_md5_init(xyz_md5ctx_t *ctx);
void xyz_md5_update(xyz_md5ctx_t *ctx, const xyz_u8_t *buf, xyz_sli_t len);
void xyz_md5_final(xyz_u8_t digest[16], xyz_md5ctx_t *ctx);
void xyz_md5_transform(xyz_u32_t buf[4], const xyz_u32_t in[16]);

int xyz_md5_file(const char *filename, xyz_u8_t digest[16]);

#endif /* __XYZ_MD5_H__ */
