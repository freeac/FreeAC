/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */


#include "xyz_int.h"
#include "xyz_str.h"

#define NSWMP_IMGDWN_PORT	"10088"

typedef struct {
	xyz_u8_t major;
	xyz_u8_t minor;
	xyz_u16_t patch;
} nsap_firm_ver_t;

typedef struct {
	xyz_u8_t deids[64];
	xyz_u8_t deidb[16];
} nsap_deid_t;

typedef struct {
	xyz_str_t *org;
	xyz_str_t *brc;
} nsap_orgbrc_t;

static  char devid[] = "00112233-4455-6677-8899-AABBCCDDEEFF\0";
static char orgid[] = "E51E9451-1902-CB1E-90AD-EEDBC2EACA89\0";
static char brcid[] = "132\0";
static  char acip[] = "211.88.76.235\0";
static char S_nsap_board_name[512] = "FreeAC-Demo-HD\0";
static char S_wtp_board_name[512] = "NS-120-bgn\0";

static nsap_firm_ver_t S_firm_ver;
static nsap_deid_t S_nsap_deid;
static nsap_firm_ver_t S_firm_ver;






const nsap_firm_ver_t *nsap_firm_version(void);
char *nsap_board_name(void);
const char *nsap_deid_get_string(void);


xyz_str_t *nsap_orgbrc_get_org(void);
xyz_str_t *nsap_orgbrc_get_brc(void);
int nsap_orgbrc_test(void);