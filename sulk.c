/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */


#include <stdio.h>
#include <netinet/in.h>
#include "xyz_log.h"
#include "wmpctrl.h"
#include "udphelp.h"
#include "wtpinfo.h"
#include "wtpnl.h"
#include "util.h"

#include "nsap_dscv_addr.h"
#include "global.h"
extern int G_wmpsock;
extern int G_nlksock;

static int S_sulktime = 10;

static int wmpSulkRecvAC(int fd);
static int wmpSulkRecvNL(int fd);

/*
 *
 */
void wmpSulk(int *state)
{
	int rc = 0;
	fd_set fset;
	struct timeval tv;
	int max_fd = G_wmpsock > G_nlksock ? G_wmpsock : G_nlksock;
	static int wifi = 0;

#if 0
	if ((1 == nsap_dscv_addr_get_off()) && (0 == wifi)) {
		if (nsap_wifi_restart() != 0) {
			xyz_log_error("start WiFi failed");
		} else {
			wifi = 1;
		}
	}
#endif

	tv.tv_sec = S_sulktime;
	tv.tv_usec = 0;

	do {
		FD_ZERO(&fset);
		FD_SET(G_wmpsock, &fset);
		FD_SET(G_nlksock, &fset);
		rc = select(max_fd + 1, &fset, NULL, NULL, &tv);
		if (rc > 0) {
			if (FD_ISSET(G_wmpsock, &fset)) {
				wmpSulkRecvAC(G_wmpsock);
			}
			if (FD_ISSET(G_nlksock, &fset)) {
				wmpSulkRecvNL(G_nlksock);
			}
		}
	} while (rc > 0);
	if (0 == rc) {
		*state = WTP_STATE_DSCV;
	} else {
		utilDoCMD("reboot");
	}
}

/*
 *
 */
static int wmpSulkRecvAC(int fd)
{
	WMMSG msg;
	struct sockaddr_in from;
	int len = 0;

	memset(&msg, 0, sizeof(msg));
	memset(&from, 0, sizeof(from));
	len = udpRecv(fd, msg.buff, WMM_BUFF_SIZE, &from);
	if (len < 1) {
		xyz_log_error("recvfrom : %s", strerror(errno));
		return -1;
	}
	return 0;
}

/*
 *
 */
static int wmpSulkRecvNL(int fd)
{
	int len = 0;
	NLBUFF nlmsg = NLBUFF_INIT;

	len = recv(fd, nlmsg.buff, NL_SELF_SIZE, 0);
	if (len < 1 ) {
		xyz_log_error("recv : %s", strerror(errno));
		return -1;
	}
	return 0;
}
