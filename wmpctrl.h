/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */

#ifndef __WMM_CTRL_H__
#define __WMM_CTRL_H__

#include <stdint.h>
#include "buff.h"
#include "wmp.h"

//
struct sec_key {
	uint32_t keylen;
	char rawkey[128];
};

struct board {
	uint8_t major;
	uint8_t minor;
	uint16_t patch;
	char hardware[128];
};

struct img_ident {
	uint8_t sessid[16];
};

struct free_url {
	uint16_t urllen;
	char *url[256];
};

struct wifi_cfg {
	uint8_t portal;
	uint8_t radiotype;
	uint8_t channel;
	uint8_t wlanindex;
	uint8_t ssidsupp;
	uint16_t power;
	uint8_t encrypt;
	uint8_t arithmetic;
	uint8_t acctenable;
	uint8_t RSSI;
	uint8_t maxassoc;
	uint8_t ssidlen;
	char ssid[40];
	uint8_t keylen;
	char key[40];
};

struct prt_cfg {
	uint32_t srvip;
	uint32_t nasip;
	uint16_t proto;
	uint16_t urllen;
	char url[512];
};


int wmmPackSecKey(BUFF *b, struct sec_key *k);
int wmmParseSecKey(BUFF *b, struct sec_key *v);

int wmmPackBoard(BUFF *b, uint8_t major, uint8_t minor, uint16_t patch, const char *name);
int wmmParseBoard(BUFF *b, struct board *v);

int wmmPackOrg(BUFF *b, const char *org, int len);
int wmmParseOrg(BUFF *b, char *v);

int wmmPackBrc(BUFF *b, const char *brc, int len);
int wmmParseBrc(BUFF *b, char *v);

int wmmParseImageIdent(BUFF *b, struct img_ident *v);

int wmmPackResCode(BUFF *b, uint32_t code);
int wmmParseResCode(BUFF *b, uint32_t *code);

int wmmPackEcho(BUFF *b, uint32_t echo);
int wmmParseEcho(BUFF *b, uint32_t *echo);

int wmmPackIdle(BUFF *b, uint32_t idle);
int wmmParseIdle(BUFF *b, uint32_t *idle);

int wmmParseWIFI(BUFF *b, struct wifi_cfg *v);

int wmmParsePortal(BUFF *b, struct prt_cfg *v);


#endif /* __WMM_CTRL_H__ */
