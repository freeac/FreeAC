/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */


#include <stdio.h>
#include "xyz_log.h"
#include "buff.h"
#include "wmpuser.h"

/*
 *
 */
int wmmPackUserInfo(BUFF *b, struct user_info *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffInit(b, WMM_ATTR_USERINFO);
	buffPutU32(b, v->sn);
	buffPutIPAddr(b, v->ip);
	buffPutBytes(b, v->mac, 6);
	buffPutU16(b, v->wlanindex);
	buffPutU32(b, v->errcode);
	return 0;
}

/*
 *
 */
int wmmParseUserInfo(BUFF *b, struct user_info *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffGetU32(b, &(v->sn));
	buffGetIPAddr(b, &(v->ip));
	buffGetByte(b, v->mac, 6);
	buffGetU16(b, &(v->wlanindex));
	buffGetU32(b, &(v->errcode));
	return ((b->offset == b->len) ? 0 : -1);
}

/*
 *
 */
int wmmPackUserAuth(BUFF *b, const char *name, int nlen, const char *pass, int plen)
{
	if (!b || !name || !pass) {
		xyz_log_error("Args error");
		return -1;
	}
	buffInit(b, WMM_ATTR_USER_NAME_PASSWORD);
	buffPutU8(b, nlen);
	buffPutStr(b, name, nlen);
	buffPutU8(b, plen);
	buffPutStr(b, pass, plen);
	return 0;
}

/*
 *
 */
int wmmParseUserAuth(BUFF *b, struct user_auth *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffGetU8(b, &(v->nlen));
	buffGetStr(b, v->name, v->nlen);
	buffGetU8(b, &(v->plen));
	buffGetStr(b, v->pass, v->plen);
	return 0;
}

/*
 *
 */
int wmmPackUserQuota(BUFF *b, struct user_quota *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffInit(b, WMM_ATTR_USERQUOTA);
	buffPutU32(b, v->sn);
	buffPutIPAddr(b, v->ip);
	buffPutBytes(b, v->mac, 6);
	buffPutU16(b, v->wlanindex);
	buffPutU32(b, v->session);
	buffPutU32(b, v->inspeed);
	buffPutU32(b, v->outspeed);
	return 0;
}

/*
 *
 */
int wmmParseUserQuota(BUFF *b, struct user_quota *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffGetU32(b, &(v->sn));
	buffGetIPAddr(b, &(v->ip));
	buffGetByte(b, v->mac, 6);
	buffGetU16(b, &(v->wlanindex));
	buffGetU32(b, &(v->session));
	buffGetU32(b, &(v->inspeed));
	buffGetU32(b, &(v->outspeed));
	return ((b->offset == b->len) ? 0 : -1);
}

/*
 *
 */
int wmmPackUserStat(BUFF *b, struct user_stat *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffInit(b, WMM_ATTR_USERSTAT);
	buffPutIPAddr(b, v->ip);
	buffPutBytes(b, v->mac, 6);
	buffPutU16(b, v->wlanindex);
	buffPutU32(b, v->online);
	buffPutU32(b, v->ingiga);
	buffPutU32(b, v->in);
	buffPutU32(b, v->outgiga);
	buffPutU32(b, v->out);
	return 0;
}

/*
 *
 */
int wmmParseUserStat(BUFF *b, struct user_stat *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffGetIPAddr(b, &(v->ip));
	buffGetByte(b, v->mac, 6);
	buffGetU16(b, &(v->wlanindex));
	buffGetU32(b, &(v->online));
	buffGetU32(b, &(v->ingiga));
	buffGetU32(b, &(v->in));
	buffGetU32(b, &(v->outgiga));
	buffGetU32(b, &(v->out));
	return ((b->offset == b->len) ? 0 : -1);
}
