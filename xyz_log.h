/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */

#ifndef __LIJIE_XYZ_LOG_H__
#define __LIJIE_XYZ_LOG_H__


#include <errno.h>

#define XYZ_LOG_LEVEL_EMESG		0
#define XYZ_LOG_LEVEL_ALERT		1
#define XYZ_LOG_LEVEL_CRIT		2
#define XYZ_LOG_LEVEL_ERROR		3
#define XYZ_LOG_LEVEL_WARN		4
#define XYZ_LOG_LEVEL_NOTICE	5
#define XYZ_LOG_LEVEL_INFO		6
#define XYZ_LOG_LEVEL_DEBUG		7


extern volatile int __xyz_log_level;

void __xyz_log_base_print(int level, const char *format, ...);
void __xyz_log_base_colour_print(int level, const char *filename, int line, const char *function,const char *format, ...);


#define XYZ_LOG_LEVEL(level) XYZ_LOG_LEVEL_##level

#ifdef __XYZ_DEBUG__
#define XYZ_LOG_BASE_PRINT(level, format, args...) __xyz_log_base_colour_print(XYZ_LOG_LEVEL(level),__FILE__, __LINE__, __FUNCTION__, format, ##args)
#else
#define XYZ_LOG_BASE_PRINT(level, format, args...) __xyz_log_base_print(XYZ_LOG_LEVEL(level), format, ##args)
#endif

#define xyz_log_debug(format, args...)	((__xyz_log_level > XYZ_LOG_LEVEL_INFO) ? XYZ_LOG_BASE_PRINT(DEBUG, format, ##args) : (void)0)

#define xyz_log_info(format, args...)	((__xyz_log_level > XYZ_LOG_LEVEL_NOTICE) ? XYZ_LOG_BASE_PRINT(INFO, format, ##args) : (void)0)

#define xyz_log_warn(format, args...)	((__xyz_log_level > XYZ_LOG_LEVEL_ERROR) ? XYZ_LOG_BASE_PRINT(WARN,  format, ##args) : (void)0)

#define xyz_log_error(format, args...)	((__xyz_log_level > XYZ_LOG_LEVEL_CRIT) ? XYZ_LOG_BASE_PRINT(ERROR, format, ##args) : (void)0)

int xyz_log_init(const char *file, unsigned int level, unsigned long int size);

void xyz_log_close(void);

#endif // __LIJIE_XYZ_LOG_H__
