/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */



#ifndef __XYZ_INT_H__
#define __XYZ_INT_H__

#ifdef __KERNEL__
#include <linux/types.h>
typedef int8_t xyz_i8_t;
typedef u_int8_t xyz_u8_t;

typedef int16_t xyz_i16_t;
typedef u_int16_t xyz_u16_t;

typedef int32_t xyz_i32_t;
typedef u_int32_t xyz_u32_t;

typedef int64_t xyz_i64_t;
typedef u_int64_t xyz_u64_t;

#else
#include <stdint.h>
typedef int8_t xyz_i8_t;
typedef uint8_t xyz_u8_t;

typedef int16_t xyz_i16_t;
typedef uint16_t xyz_u16_t;

typedef int32_t xyz_i32_t;
typedef uint32_t xyz_u32_t;

typedef int64_t xyz_i64_t;
typedef uint64_t xyz_u64_t;

#endif

typedef signed long int xyz_sli_t;
typedef unsigned long int xyz_uli_t;

#endif /* __XYZ_INT_H__ */
