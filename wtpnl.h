/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */



#ifndef __NATSHELL_WTP_NTLNK_H__
#define __NATSHELL_WTP_NTLNK_H__

#include <stdint.h>
#include "buff.h"

#define NL_SELF_PROTO	29
#define NL_SELF_SIZE	4096


#define NLMSG_TYPE_HOOK_OPEN		1
#define NLMSG_TYPE_HOOK_CLOSE		2
#define NLMSG_TYPE_HOOK_PAUSE		3
#define NLMSG_TYPE_HOOK_RESTART		4

/*  网络接口    */
#define NLMSG_TYPE_WAN_IF			7
#define NLMSG_TYPE_LAN_IF			8

#define NLMSG_TYPE_PRT_CFG			10
#define NLMSG_TYPE_IDLE_TIME		11
#define NLMSG_TYPE_FREE_V4IP		12
#define NLMSG_TYPE_APID				13
#define NLMSG_TYPE_WIFI_CFG			14
#define NLMSG_TYPE_ACCT_TIME		15
#define NLMSG_TYPE_FREE_URL			16

#define NLMSG_TYPE_ORGBRC		20

#define NLMSG_TYPE_NEWUSR_REQ	50
#define NLMSG_TYPE_NEWUSR_REP	51
#define NLMSG_TYPE_USRACCR_REQ	52
#define NLMSG_TYPE_USRACCR_REP	53
#define NLMSG_TYPE_USRLEFT_REQ	54
#define NLMSG_TYPE_USRLEFT_REP	55
#define NLMSG_TYPE_USRDROP_REQ	56
#define NLMSG_TYPE_USRDROP_REP	57
#define NLMSG_TYPE_USRACCT_REQ	58
#define NLMSG_TYPE_USRACCT_REP	59
#define NLMSG_TYPE_GETACCR_REQ	60
#define NLMSG_TYPE_GETACCR_REP	61

///////////////////////////////////////////////////////////////////////////////

#define NLMSG_ATTR_HOOK_OPEN	1
#define NLMSG_ATTR_HOOK_CLOSE	2
#define NLMSG_ATTR_HOOK_PAUSE	3
#define NLMSG_ATTR_HOOK_RESTART	4

#define NLMSG_ATTR_NIF_WAN		5
#define NLMSG_ATTR_NIF_LAN		6

#define NLMSG_ATTR_PRT_CFG		10
#define NLMSG_ATTR_IDLE_TIME	11
#define NLMSG_ATTR_FREE_V4IP	12
#define NLMSG_ATTR_APID			13
#define NLMSG_ATTR_WIFI_CFG		14
#define NLMSG_ATTR_ACCT_TIME	15
#define NLMSG_ATTR_GETACCR		16
#define NLMSG_ATTR_FREE_URL		17

#define NLMSG_ATTR_ORG_ID		20
#define NLMSG_ATTR_BRC_ID		21

#define NLMSG_ATTR_USERINFO		50
#define NLMSG_ATTR_USERQUOTA	51
#define NLMSG_ATTR_USERSTAT		52

#define FREE_ADDRESS_SIZE	64

#define FREE_URL_TABLE_SIZE		4
#define FREE_URL_MAX_LEN		128

////////////////////////////////////////////////////////////////////////

#define NLBUFF_INIT		{0, NL_SELF_SIZE, 0, {0}}

typedef struct nlmsgbuff {
	uint16_t type;
	uint16_t len;
	int offset;
	char buff[NL_SELF_SIZE];
} NLBUFF;



int wtpnlNewConnect(void);

int wtpnlSend(uint16_t type, void *msg, int len);

static inline int wtpnlSendNLBuff(NLBUFF *b)
{
    return 0;
//	return wtpnlSend(b->type, b->buff, b->offset);
}

int wtpnlRcvdPrep(NLBUFF *nlmsg);

int wtpnlTimeRecv(const int fd, int to, NLBUFF *buff);
//////////////////////////////////////////////////////////////////////////////

int nlbuffRawPut(NLBUFF *buff, void *value, int len);

int nlbuffRawGet(NLBUFF *buff, void *value, int len);

int nlbuffGetTLV(NLBUFF *buff, BUFF *t);

static inline int nlbuffPutU8(NLBUFF *buff, uint8_t value)
{
	return nlbuffRawPut(buff, &value, sizeof(uint8_t));
}

static inline int nlbuffPutU16(NLBUFF *buff, uint16_t value)
{
	return nlbuffRawPut(buff, &value, sizeof(uint16_t));
}

static inline int nlbuffPutU32(NLBUFF *buff, uint32_t value)
{
	return nlbuffRawPut(buff, &value, sizeof(uint32_t));
}

static inline int nlbuffPutBytes(NLBUFF *buff, uint8_t *value, int len)
{
	return nlbuffRawPut(buff, value, len);
}

static inline int nlbuffPutStr(NLBUFF *buff, const char *value, int len)
{
	return nlbuffRawPut(buff, (void *)value, len);
}

///////////////////////////////////////////////////////////////////////////////////

static inline int nlbuffGetU8(NLBUFF *buff, uint8_t *value)
{
	return nlbuffRawGet(buff, value, sizeof(uint8_t));
}

static inline int nlbuffGetU16(NLBUFF *buff, uint16_t *value)
{
	return nlbuffRawGet(buff, value, sizeof(uint16_t));
}

static inline int nlbuffGetU32(NLBUFF *buff, uint32_t *value)
{
	return nlbuffRawGet(buff, value, sizeof(uint32_t));
}

static inline int nlbuffGetBytes(NLBUFF *buff, uint8_t *value, int len)
{
	return nlbuffRawGet(buff, value, len);
}

static inline int nlbuffGetStr(NLBUFF *buff, char *value, int len)
{
	return nlbuffRawGet(buff, value, len);
}


///////////////////////////////////////////////////////////////////////////////

int wtpnlRecv(const int fd);

int wtpnlmsgHandleIdle(BUFF *msg);
int wtpnlmsgHandleAcct(BUFF *msg);
int wtpnlmsgHandlePortal(BUFF *msg);
int wtpnlmsgHandleWiFi(BUFF *msg);

int wtpnlmsgHandleOrg(BUFF *msg);
int wtpnlmsgHandleBrc(BUFF *msg);

int wtpnlmsgHandleFreeIP(BUFF *msg);
int wtpnlmsgHandleFreeURL(BUFF *msg);

int wtpnlmsgHandleNewUserReq(NLBUFF *msg);
int wtpnlmsgHandleNewUserRep(BUFF *msg);

int wtpnlmsgHandleUserAccrReq(BUFF *msg);
int wtpnlmsgHandleUserAccrRep(NLBUFF *msg);

int wtpnlmsgHandleUserAcctReq(NLBUFF *msg);

int wtpnlmsgHandleUserLeftReq(NLBUFF *msg);

int wtpnlmsgHandleUserDropReq(BUFF *msg);
int wtpnlmsgHandleUserDropRep(NLBUFF *msg);

#endif /* __NATSHELL_WTP_NTLNK_H__ */
