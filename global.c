/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */


#include "global.h"
#include "xyz_log.h"





const char *nsap_deid_get_string(void)
{
    strncpy(S_nsap_deid.deids,devid,sizeof(devid));
	return S_nsap_deid.deids;
}

char *nsap_board_name(void)
{

	memset(S_nsap_board_name, 0, sizeof(S_nsap_board_name));
        strncpy(S_nsap_board_name,S_nsap_board_name,sizeof(S_nsap_board_name));
	return S_nsap_board_name;
}

const nsap_firm_ver_t *nsap_firm_version(void)
{
    S_firm_ver.major = 1;
    S_firm_ver.minor = 0;
    S_firm_ver.patch = 8;
	return &S_firm_ver;
}



