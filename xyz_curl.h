/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */

#ifndef __XYZ_CURL_H__
#define __XYZ_CURL_H__

#include <stdio.h>
#include <curl/curl.h>

typedef struct {
	const char *filename;
	FILE *stream;
}xyz_curl_file_t;

CURL *xyz_curl_easy_init(void);
void xyz_curl_easy_close(CURL *ch);

int xyz_curl_easy_GET_to_mem(CURL *ch, const char *url, void **out, double *size);
int xyz_curl_easy_GET_to_file(CURL *ch, const char *url, const char *file, double *size);
int xyz_curl_easy_GET_to_stream(CURL *ch, const char *url, FILE *fp, double *size);
int xyz_curl_easy_GET_to_fno(CURL *ch, const char *url, int fd, double *size);



#endif /* __XYZ_CURL_H__ */
