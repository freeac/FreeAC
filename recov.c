/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */

#include <stdio.h>
#include <unistd.h>
#include <netinet/in.h>
#include "xyz_log.h"
#include "wmpctrl.h"
#include "udphelp.h"
#include "wtpinfo.h"
#include "wtpnl.h"
#include "util.h"
#include "global.h"


extern int G_wmpsock;
extern int G_nlksock;
extern struct sockaddr_in G_wmpaddr;


static int wmpRecovSendReq(NLBUFF *accr);
static int wmpRecovRcvAll(int to);
static int wmpRecovRcvAC(int fd);
static int wmpRecovRcvNL(int fd);
static int wtpRecovGetAccrUser(NLBUFF *rep);

/*
 *
 */
void wmpRecov(int *state)
{
	int i = 0;
	NLBUFF nlmsg = NLBUFF_INIT;

	if (wtpRecovGetAccrUser(&nlmsg)) {
		xyz_log_error("Handle Get-ACCR-REQ error, after 10s, will reboot!!!");
		sleep(10);
		xyz_log_error("Now Reboot!!!");
		utilDoCMD("reboot");
	}
	for (; i < 2; ++i) {
		wmpRecovSendReq(&nlmsg);
		if (0 == wmpRecovRcvAll(5)) {
		//	system("/sbin/ns_led.sh running &");
			*state = WTP_STATE_RUN;
			if (wtpInfoSendHookRestart2Krn()) {
				xyz_log_error("Restart HOOK error");
			}
			return;
		}
	}
	xyz_log_error("RECOV-State timeout, after 10s, will reboot!!!");
	sleep(10);
	xyz_log_error("Now Reboot!!!");
	utilDoCMD("reboot");
}

/*
 *
 */
static int wmpRecovSendReq(NLBUFF *accr)
{
	WMMSG msg;
	uint16_t seqnum = 0;
	uint8_t *apid = NULL;

//	apid = (uint8_t *)nsap_deid_get_byte();
        apid = (uint8_t *)devid;
	seqnum = wtpInfoGetSEQNUM();
	if (wmmClearInit(&msg, apid, seqnum, WMM_CODE_RECOV_REQ)) {
		xyz_log_error("Message head INIT error");
		return -1;
	}
	if (accr->len > (WMM_BUFF_SIZE - msg.offset)) {
		xyz_log_error("Attribute is too long");
		return -1;
	}
	memcpy(&(msg.buff[msg.offset]), &(accr->buff[accr->offset]), accr->len);
	wmmSetOffset(&msg, msg.offset + accr->len);
	if (udpSend(G_wmpsock, msg.buff, msg.offset, &G_wmpaddr) < 1) {
		xyz_log_error("sendto : %s", strerror(errno));
		return -1;
	}
	return 0;
}

/*
 *
 */
static int wmpRecovRcvAll(int to)
{
	int rc = 0;
	fd_set fset;
	struct timeval tv;
	int max_fd = G_wmpsock > G_nlksock ? G_wmpsock : G_nlksock;

	tv.tv_sec = to;
	tv.tv_usec = 0;

	do {
		FD_ZERO(&fset);
		FD_SET(G_wmpsock, &fset);
		FD_SET(G_nlksock, &fset);
		rc = select(max_fd + 1, &fset, NULL, NULL, &tv);
		if (rc > 0) {
			if (FD_ISSET(G_wmpsock, &fset)) {
				if (0 == wmpRecovRcvAC(G_wmpsock)) {
					return 0;
				}
			}
			if (FD_ISSET(G_nlksock, &fset)) {
				wmpRecovRcvNL(G_nlksock);
			}
		}
	} while (rc > 0);

	return -1;
}


/*
 *
 */
static int wmpRecovRcvAC(int fd)
{
	WMMSG msg;
	struct sockaddr_in from;
	int len = 0;
	BUFF attr;
	uint32_t rescode = 0;

	memset(&msg, 0, sizeof(msg));
	memset(&from, 0, sizeof(from));
	len = udpRecv(fd, msg.buff, WMM_BUFF_SIZE, &from);
	if (len < 1) {
		xyz_log_error("recvfrom : %s", strerror(errno));
		return -1;
	}
	msg.totlen = len;
	if (wmmPrepRecvd(&msg)) {
		xyz_log_error("Preprocess message error");
		return -1;
	}
	if (WMM_CODE_RECOV_REP == msg.code) {
		if (1 == wmmGetAttr(&msg, &attr)) {
			if (WMM_ATTR_RESCODE == attr.type) {
				if (wmmParseResCode(&attr, &rescode)) {
					xyz_log_error("Parse RESULT-CODE error");
					return -1;
				} else {
					if (WMM_RESULT_CODE_OK == rescode) {
						return 0;
					}
				}
			}
		}
	}
	return -1;
}

/*
 *
 */
static int wtpRecovGetAccrUser(NLBUFF *rep)
{
	NLBUFF nlmsg = NLBUFF_INIT;
	BUFF attr;
	int len = 0;

	nlmsg.type = NLMSG_TYPE_GETACCR_REQ;

	buffInit(&attr, NLMSG_ATTR_GETACCR);

	if (nlbuffPutBuff(&nlmsg, &attr)) {
		xyz_log_error("NETLINK Buffer Put ATTR error");
		return -1;
	}
	if (wtpnlSendNLBuff(&nlmsg)) {
		xyz_log_error("Send Get ACCR User to kernel error");
		return -1;
	}
	if (wtpnlTimeRecv(G_nlksock, 1, rep)) {
		xyz_log_error("Recv ACCR-User error");
		return -1;
	}
	if (rep->type != NLMSG_TYPE_GETACCR_REP) {
		xyz_log_error("This RCVD message is not ACCR-User");
		return -1;
	}
	return 0;
}

/*
 *
 */
static int wmpRecovRcvNL(int fd)
{
	int len = 0;
	NLBUFF nlmsg = NLBUFF_INIT;

	len = recv(fd, nlmsg.buff, NL_SELF_SIZE, 0);
	if (len < 1 ) {
		xyz_log_error("recv : %s", strerror(errno));
		return -1;
	}
	return 0;
}
