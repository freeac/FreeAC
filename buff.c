/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */


#include <stdio.h>
#include <string.h>
#include "xyz_log.h"
#include "buff.h"

int buffInit(BUFF *b, uint16_t type)
{
	if (!b) {
		xyz_log_error("Arg error");
	}
	memset(b, 0, sizeof(BUFF));
	b->type = type;
	b->len = BUFF_SIZE;
	return 0;
}

/*
 *
 */
int buffRawPut(BUFF *b, uint8_t type, const void *value, int len)
{
	if (!b || !value || len < 1) {
		xyz_log_error("Args error");
		return -1;
	}
	if (b->offset + len > BUFF_SIZE) {
		xyz_log_error("Maximum message length exceeded");
		return -1;
	}
	if (BUFF_TYPE_U16 == type) {
		uint16_t tmp = *(uint16_t *)value;
		uint16_t tmp16 = htons(tmp);
		if (!memcpy(&(b->buff[b->offset]), &tmp16, sizeof(uint16_t))) {
			xyz_log_error("memcpy : %s", strerror(errno));
			return -1;
		}
	} else if (BUFF_TYPE_U32 == type) {
		uint32_t tmp = *(uint32_t *)value;
		uint32_t tmp32 = htonl(tmp);
		if (!memcpy(&(b->buff[b->offset]), &tmp32, sizeof(uint32_t))) {
			xyz_log_error("memcpy : %s", strerror(errno));
			return -1;
		}
	} else {
		if (!memcpy(&(b->buff[b->offset]), value, len)) {
			xyz_log_error("memcpy : %s", strerror(errno));
			return -1;
		}
	}
	b->offset += len;
	b->len = b->offset;
	return 0;
}

/*
 *
 */
int buffRawGet(BUFF *b, uint8_t type, void *value, int len)
{
	if (!b || !value || len < 0) {
		xyz_log_error("Args error");
		return -1;
	}
	if (0 == len) {
		return 0;
	}
	if (b->offset + len > b->len) {
		xyz_log_error("Maximum message length exceeded");
		return -1;
	}

	if (BUFF_TYPE_U16 == type) {
		uint16_t tmp = 0;
		if (!memcpy(&tmp, &(b->buff[b->offset]), sizeof(uint16_t))) {
			xyz_log_error("memcpy : %s", strerror(errno));
			return -1;
		}
		tmp = ntohs(tmp);
		memcpy(value, &tmp, sizeof(uint16_t));
	} else if (BUFF_TYPE_U32 == type) {
		uint32_t tmp = 0;
		if (!memcpy(&tmp, &(b->buff[b->offset]), sizeof(uint32_t))) {
			xyz_log_error("memcpy : %s", strerror(errno));
			return -1;
		}
		tmp = ntohl(tmp);
		memcpy(value, &tmp, sizeof(uint32_t));
	} else {
		if (!memcpy(value, &(b->buff[b->offset]), len)) {
			xyz_log_error("memcpy : %s", strerror(errno));
			return -1;
		}
	}
	b->offset += len;
	return 0;
}
