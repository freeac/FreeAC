/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include "xyz_log.h"
#include "util.h"

int utilSetNonblock(int fd)
{
	int flags = 0;
	if (fd < 0) {
		xyz_log_error("argument is error");
		return -1;
	}
	if ((flags = fcntl(fd, F_GETFL, 0)) == -1) {
		xyz_log_error("fcntl : %s", strerror(errno));
		return -1;
	} else if (fcntl(fd, F_SETFL, flags | O_NONBLOCK | O_ASYNC) != 0) {
		xyz_log_error("fcntl : %s", strerror(errno));
		return -1;
	}
	return 0;
}



int utilDoCMD(const char *cmd)
{
	int rc = 0;
	FILE *fp = NULL;

	if (!cmd) {
		xyz_log_error("Arg error");
		return -1;
	}

	fp = popen(cmd, "r");
	if (!fp) {
		xyz_log_error("popen : %s", strerror(errno));
		return -1;
	}
	rc = pclose(fp);

	rc = WEXITSTATUS(rc);
	if (0 == rc) {
		return 0;
	} else {
		xyz_log_error("Do \"%s\" result failed, code %d", cmd, rc);
		return -1;
	}
	return 0;
}

int utilByteMAC(const char *s, uint8_t *m)
{
	int rc = 0;
	char p = 0;
	static char *template = "%02X%[:-]%02X%[:-]%02X%[:-]%02X%[:-]%02X%[:-]%02X";


	if (!s || !m) {
		xyz_log_error("Args error");
		return -1;
	}
	if (strlen(s) != 17) {
		xyz_log_error("String MAC format is error");
		return -1;
	}

	rc = sscanf(s, template,
			&(m[0]), &p,
			&(m[1]), &p,
			&(m[2]), &p,
			&(m[3]), &p,
			&(m[4]), &p,
			&(m[5]));

	if (rc != 11) {
		xyz_log_error("sscanf : %s", strerror(errno));
		rc = -1;
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////////////

#if 0

int main(int argc, char **argv)
{
	char *s1 = "00-01-02-03-04-05";
	char *s2 = "00:01:02:03:04:05";
	char *s3 = "AA-01-02-03-04-05";
	char *s4 = "00-01-02-aa-04-05";

	uint8_t mac[6] = {0};

	logOpen("stdout", 7);

	if (utilByteMAC(s1, mac)) {
		xyz_log_error("MAC parse error");
		return -1;
	}
	logDebug("%02X:%02X:%02X:%02X:%02X:%02X", mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
	memset(mac, 0, sizeof(mac));

	if (utilByteMAC(s2, mac)) {
		xyz_log_error("MAC parse error");
		return -1;
	}
	logDebug("%02X:%02X:%02X:%02X:%02X:%02X", mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
	memset(mac, 0, sizeof(mac));

	if (utilByteMAC(s3, mac)) {
		xyz_log_error("MAC parse error");
		return -1;
	}
	logDebug("%02X:%02X:%02X:%02X:%02X:%02X", mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
	memset(mac, 0, sizeof(mac));

	if (utilByteMAC(s4, mac)) {
		xyz_log_error("MAC parse error");
		return -1;
	}
	logDebug("%02X:%02X:%02X:%02X:%02X:%02X", mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
	memset(mac, 0, sizeof(mac));
	return 0;
}

#endif
