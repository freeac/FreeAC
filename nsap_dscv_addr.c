/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */

#include <stdio.h>
#include <string.h>
#include "xyz_log.h"
#include "xyz_int.h"

#include "nsap_dscv_addr.h"
#include "global.h"

typedef struct {
	char addr[40];
	xyz_u8_t offline;
} nsap_dscv_addr_t;

static nsap_dscv_addr_t S_nsap_dscv_addr;

/*初始化AC的地址*/
int nsap_dscv_addr_init(void)
{
	char buf[512] = { 0 };
	char acaddr[] = "natshell.AC.ac_ipaddr";
	char offline[] = "natshell.AC.off_line";
	int len;

	memset(&S_nsap_dscv_addr, 0, sizeof(nsap_dscv_addr_t));
        /* 从UCI获取AC的IP地址，本程序省略了这个过程。直接采用 ac.freeac.cn的地址作为AC地址。
	if (xyz_uci_get_option(acaddr, buf, sizeof(buf))) {
		xyz_log_error("-NSAP-DSCV- read AC address failed");
		return -1;
	}
*/
        strncpy(buf,acip,sizeof(acip));
	xyz_log_debug("-NSAP-DSCV- AC address : %s", buf);
	len = strlen(buf);
	if (len > 39) {
		xyz_log_debug("-NSAP-DSCV- AC address \"%s\" length %d bytes is so long", buf, len);
		return -1;
	}
	strncpy(S_nsap_dscv_addr.addr, buf, len);
	memset(buf, 0, sizeof(buf));
        /*
	if (xyz_uci_get_option(offline, buf, sizeof(buf))) {
		xyz_log_error("-NSAP-DSCV- read off-line failed");
		memset(&S_nsap_dscv_addr, 0, sizeof(nsap_dscv_addr_t));
		return -1;
	}
         */
        strncpy(buf,acip,sizeof(acip));
	xyz_log_debug("-NSAP-DSCV- off-line %s", buf);
	if (('0' == buf[0]) && ('\0' == buf[1])) {
		S_nsap_dscv_addr.offline = 0;
	} else {
		S_nsap_dscv_addr.offline = 1;
	}
	return 0;
}

const char *nsap_dscv_addr_get_addr(void)
{
	return S_nsap_dscv_addr.addr;
}

int nsap_dscv_addr_get_off(void)
{
	return S_nsap_dscv_addr.offline;
}
/*
int nsap_dscv_addr_set(const char *addr, unsigned int off)
{
	char buf[512] = { 0 };

	if (!addr) {
		xyz_log_error("-NSAP-DSCV- function input args error");
		return -1;
	}
	if (snprintf(buf, (sizeof(buf) - 1), "natshell.AC.ac_ipaddr=%s", addr)) {
		xyz_log_error("-NSAP-DSCV- snprintf failed, %s", strerror(errno));
		return -1;
	}
	xyz_log_debug("-NSAP-DSCV- UCI CMD \"%s\"", buf);
	if (xyz_uci_do_section_cmd(NULL, XYZ_UCI_CMD_SET, buf)) {
		xyz_log_error("-NSAP-DSCV- UCI do \"%s\" failed", buf);
		return -1;
	}
	memset(buf, 0, sizeof(buf));
	if (snprintf(buf, (sizeof(buf) - 1), "natshell.AC.off_line=%d", off)) {
		xyz_log_error("-NSAP-DSCV- snprintf failed, %s", strerror(errno));
		return -1;
	}
	xyz_log_debug("-NSAP-DSCV- UCI CMD \"%s\"", buf);
	if (xyz_uci_do_section_cmd(NULL, XYZ_UCI_CMD_SET, buf)) {
		xyz_log_error("-NSAP-DSCV- UCI do \"%s\" failed", buf);
		return -1;
	}
	return 0;
}
 */