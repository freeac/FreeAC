/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */


#include <stdio.h>
#include <stdint.h>
#include <netinet/in.h>
#include "xyz_log.h"
#include "wtpinfo.h"
#include "wtpnl.h"
#include "global.h"

extern struct sockaddr_in G_wmpaddr;
extern nsap_orgbrc_t S_nsap_orgbrc ;

static uint32_t S_idle;
static uint32_t S_echo;
static uint32_t S_acct;
static char S_acaddr[20] = { 0 };

static struct wifi_cfg S_wifi;
static struct prt_cfg S_portal;

extern xyz_str_t *nsap_orgbrc_get_brc(void);
extern xyz_str_t *nsap_orgbrc_get_org(void);

int wtpInfoSetBoardName(const char *v, int len)
{
/*
	if (!v || len > sizeof(S_wtp_board_name) - 1) {
		return -1;
	}
	memset(S_wtp_board_name, 0, sizeof(S_wtp_board_name));
	strncpy(S_wtp_board_name, v, len);
 */
	return 0;
}

const char *wtpInfoGetBoardName(void)
{
    xyz_log_error("S_wtp_board_name = %s", S_wtp_board_name);
   
	return S_wtp_board_name;
}


/*
 *
 */
uint32_t wtpInfoSetIdle(uint32_t idle)
{
	S_idle = idle;
	return S_idle;
}

/*
 *
 */
uint32_t wtpInfoGetIdle(void)
{
	return S_idle;
}

/*
 *
 */
uint32_t wtpInfoSetEcho(uint32_t echo)
{
	S_echo = echo;
	return S_echo;
}

/*
 *
 */
uint32_t wtpInfoGetEcho(void)
{
	return S_echo;
}

/*
 *
 */
uint32_t wtpInfoSetAcct(uint32_t acct)
{
	S_acct = acct;
	return S_acct;
}

/*
 *
 */
uint32_t wtpInfoGetAcct(void)
{
	return S_acct;
}

/*
 *
 */
struct wifi_cfg *wtpInfoGetWIFI(void)
{
	return &S_wifi;
}

/*
 *
 */
struct prt_cfg *wtpInfoGetPortal(void)
{
	return &S_portal;
}

/*
 *
 */
const char *wtpinfoGetACIPStr(void)
{
	return S_acaddr;
}

/*
 *
 */
const char *wtpinfoSetACIPStr(const char *s)
{
	memset(S_acaddr, 0, sizeof(S_acaddr));
	strncpy(S_acaddr, s, sizeof(S_acaddr) - 1);
	return S_acaddr;
}

uint16_t wtpInfoGetSEQNUM(void)
{
	static uint16_t seq_num = 65535;

	if (65535 == seq_num) {
		seq_num = random() % 65536;
	} else {
		seq_num++;
	}
	return seq_num;
}


////////////////////////////////////////////////////////////////////

int wtpInfoSendApid2Krn(void)
{
	NLBUFF nlmsg = NLBUFF_INIT;
	BUFF attr;
	char *apid = NULL;

	apid = (char *)nsap_deid_get_string();

	nlmsg.type = NLMSG_TYPE_APID;

	buffInit(&attr, NLMSG_ATTR_APID);

	if (buffPutStr(&attr, apid, strlen(apid))) {
		xyz_log_error("Buffer put WTP-ID string error");
		return -1;
	}
/*
	if (nlbuffPutBuff(&nlmsg, &attr)) {
		xyz_log_error("NETLINK Buffer Put ATTR error");
		return -1;
	}
	if (wtpnlSendNLBuff(&nlmsg)) {
		xyz_log_error("Send APID to kernel error");
		return -1;
	}
 */
	return 0;
}

int wtpInfoSendOrgBrc2Krn(void)
{
	NLBUFF nlmsg = NLBUFF_INIT;
	BUFF attr;
	xyz_str_t *id = NULL;



	nlmsg.type = NLMSG_TYPE_ORGBRC;

	id = nsap_orgbrc_get_org();
        
	buffInit(&attr, NLMSG_ATTR_ORG_ID);
	if (buffPutStr(&attr, id->data, id->len)) {
		xyz_log_error("Buffer put ORG-ID string error");
		return -1;
	}
        /*
	if (nlbuffPutBuff(&nlmsg, &attr)) {
		xyz_log_error("NETLINK Buffer Put ATTR error");
		return -1;
	}
*/
	id = nsap_orgbrc_get_brc();
	buffInit(&attr, NLMSG_ATTR_BRC_ID);
	if (buffPutStr(&attr, id->data, id->len)) {
		xyz_log_error("Buffer put BRC-ID string error");
		return -1;
	}
        /*
	if (nlbuffPutBuff(&nlmsg, &attr)) {
		xyz_log_error("NETLINK Buffer Put ATTR error");
		return -1;
	}

	if (wtpnlSendNLBuff(&nlmsg)) {
		xyz_log_error("Send APID to kernel error");
		return -1;
	}
 */
        
        	
	
	return 0;
}


/*
 *
 */
int wtpInfoSendIdle2Krn(void)
{
	NLBUFF nlmsg = NLBUFF_INIT;
	BUFF attr;

	nlmsg.type = NLMSG_TYPE_IDLE_TIME;

	buffInit(&attr, NLMSG_ATTR_IDLE_TIME);

	buffPutU32(&attr, S_idle);
/*
	if (nlbuffPutBuff(&nlmsg, &attr)) {
		xyz_log_error("NETLINK Buffer Put ATTR error");
		return -1;
	}
	if (wtpnlSendNLBuff(&nlmsg)) {
		xyz_log_error("Send IDLE time to kernel error");
		return -1;
	}
 */
	return 0;
}

/*
 *
 */
int wtpInfoSendAcct2Krn(void)
{
	NLBUFF nlmsg = NLBUFF_INIT;
	BUFF attr;

	nlmsg.type = NLMSG_TYPE_ACCT_TIME;

	buffInit(&attr, NLMSG_ATTR_ACCT_TIME);

	buffPutU32(&attr, S_acct);
/*
	if (nlbuffPutBuff(&nlmsg, &attr)) {
		xyz_log_error("NETLINK Buffer Put ATTR error");
		return -1;
	}
	if (wtpnlSendNLBuff(&nlmsg)) {
		xyz_log_error("Send ACCT time to kernel error");
		return -1;
	}
 */ 
	return 0;
}

/*
 *
 */
int wtpInfoSendPrt2Krn(void)
{
	NLBUFF nlmsg = NLBUFF_INIT;
	BUFF attr;

	nlmsg.type = NLMSG_TYPE_PRT_CFG;

	buffInit(&attr, NLMSG_ATTR_PRT_CFG);
	buffPutIPAddr(&attr, S_portal.srvip);
	buffPutIPAddr(&attr, S_portal.nasip);
	buffPutU16(&attr, S_portal.proto);
	buffPutU16(&attr, S_portal.urllen);
	if (buffPutStr(&attr, S_portal.url, S_portal.urllen)) {
		xyz_log_error("Buffer put portal URL error");
		return -1;
	}
/*
	if (nlbuffPutBuff(&nlmsg, &attr)) {
		xyz_log_error("NETLINK Buffer Put ATTR error");
		return -1;
	}
	if (wtpnlSendNLBuff(&nlmsg)) {
		xyz_log_error("Send portal set to kernel error");
		return -1;
	}
 */
	return 0;
}

/*
 *
 */
int wtpInfoSendHookOpen2Krn(uint32_t method, const char *dev)
{
	NLBUFF nlmsg = NLBUFF_INIT;
	BUFF attr;

	if (!dev) {
		xyz_log_error("Arg error");
		return -1;
	}
	nlmsg.type = NLMSG_TYPE_HOOK_OPEN;

	buffInit(&attr, NLMSG_ATTR_HOOK_OPEN);
	buffPutU32(&attr, method);
	buffPutStr(&attr, dev, strlen(dev));
/*
	if (nlbuffPutBuff(&nlmsg, &attr)) {
		xyz_log_error("NETLINK Buffer Put ATTR error");
		return -1;
	}
	if (wtpnlSendNLBuff(&nlmsg)) {
		xyz_log_error("Send HOOK open to kernel error");
		return -1;
	}
 */
	return 0;
}

/*
 *
 */
int wtpInfoSendHookClose2Krn(void)
{
	NLBUFF nlmsg = NLBUFF_INIT;
	BUFF attr;

	nlmsg.type = NLMSG_TYPE_HOOK_CLOSE;

	buffInit(&attr, NLMSG_ATTR_HOOK_CLOSE);
/*
	if (nlbuffPutBuff(&nlmsg, &attr)) {
		xyz_log_error("NETLINK Buffer Put ATTR error");
		return -1;
	}
	if (wtpnlSendNLBuff(&nlmsg)) {
		xyz_log_error("Send HOOK Close to kernel error");
		return -1;
	}
 */
	return 0;
}

/*
 *
 */
int wtpInfoSendHookPause2Krn(void)
{
	NLBUFF nlmsg = NLBUFF_INIT;
	BUFF attr;

	nlmsg.type = NLMSG_TYPE_HOOK_PAUSE;

	buffInit(&attr, NLMSG_ATTR_HOOK_PAUSE);
/*
	if (nlbuffPutBuff(&nlmsg, &attr)) {
		xyz_log_error("NETLINK Buffer Put ATTR error");
		return -1;
	}
	if (wtpnlSendNLBuff(&nlmsg)) {
		xyz_log_error("Send HOOK pause to kernel error");
		return -1;
	}
 */
	return 0;
}

/*
 *
 */
int wtpInfoSendHookRestart2Krn(void)
{

    /*
      NLBUFF nlmsg = NLBUFF_INIT;
	BUFF attr;

	nlmsg.type = NLMSG_TYPE_HOOK_RESTART;

	buffInit(&attr, NLMSG_ATTR_HOOK_RESTART);

	if (nlbuffPutBuff(&nlmsg, &attr)) {
		xyz_log_error("NETLINK Buffer Put ATTR error");
		return -1;
	}
	if (wtpnlSendNLBuff(&nlmsg)) {
		xyz_log_error("Send HOOK restart to kernel error");
		return -1;
	}
     */
	return 0;
}

