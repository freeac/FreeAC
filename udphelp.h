/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */

#ifndef __LIJIE_UDP_HELP_H__
#define __LIJIE_UDP_HELP_H__

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>

static inline int udpSend(int fd, void *buffer, int size,
		struct sockaddr_in *addr) {
	return sendto(fd, buffer, size, 0, (struct sockaddr *) addr,
			sizeof(struct sockaddr_in));
}

static inline int udpRecv(int fd, void *buffer, int size,
		struct sockaddr_in *addr) {
	int addrlen = sizeof(struct sockaddr_in);
	return recvfrom(fd, buffer, size, 0, (struct sockaddr *) addr, &addrlen);
}

static inline int udpCloseConnect(int fd) {
	return close(fd);
}

int udpNewConnect(int nonblock, long msto);

int udpNewLongConnect(const char *sip, const char *sport, const char *dip,
		const char *dport, int nonblock, unsigned long msto);

static inline int udpNewBlockConnect(long msto) {
	return udpNewConnect(0, msto);
}

static inline int udpNewNonblockConnect(void) {
	return udpNewConnect(1, 0);
}

static inline int udpNewBlockLongConnect(const char *sip, const char *sport,
		const char *dip, const char *dport, unsigned long msto) {
	return udpNewLongConnect(sip, sport, dip, dport, 0, msto);
}

static inline int udpNewNonblockLongConnect(const char *sip, const char *sport,
		const char *dip, const char *dport) {
	return udpNewLongConnect(sip, sport, dip, dport, 1, 0);
}

int udpNewServer(const char *ip, uint16_t port, int nonblock, long msto);

static inline int udpNewBlockServer(const char *ip, uint16_t port, long msto)
{
	return udpNewServer(ip, port, 0, msto);
}

static inline int udpNewNonblockServer(const char *ip, uint16_t port)
{
	return udpNewServer(ip, port, 1, 0);
}

int udpTimeRecv(int fd, void *buff, int size, struct sockaddr_in *from, int msec);

#endif /* __LIJIE_UDP_HELP_H__ */
