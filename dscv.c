/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */


#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include "xyz_log.h"
#include "wmpctrl.h"
#include "udphelp.h"
#include "wtpinfo.h"
#include "global.h" 

extern int G_wmpsock;
extern struct sockaddr_in G_wmpaddr;
extern uint16_t wtpInfoGetSEQNUM(void);
extern const char *wtpInfoGetBoardName(void);
extern xyz_str_t *nsap_orgbrc_get_brc(void);
extern nsap_orgbrc_t S_nsap_orgbrc ;

xyz_str_t *nsap_orgbrc_get_org(void)
{
  
 // S_nsap_orgbrc.org->len = strlen(orgid);
	return S_nsap_orgbrc.org;
}

xyz_str_t *nsap_orgbrc_get_brc(void)
{ 
    
 //  S_nsap_orgbrc.brc->len = strlen(brcid);
	return S_nsap_orgbrc.brc;
}

int nsap_orgbrc_test(void)
{
    nsap_orgbrc_get_brc();
    xyz_log_debug("brcid is : %s",(char *)S_nsap_orgbrc.brc->data);
	if (S_nsap_orgbrc.org && S_nsap_orgbrc.brc) {
		return !(xyz_str_is_valid(S_nsap_orgbrc.org) && xyz_str_is_valid(S_nsap_orgbrc.brc));
	}
	return -1;
}

void wmpDscv(int *state)
{
	WMMSG reqmsg;
	WMMSG repmsg;
	BUFF attr;
	struct sockaddr_in from;
	int len = 0;
	uint16_t seqnum = 0;
	uint8_t *apid = NULL;

	if (G_wmpsock > 0) {
		udpCloseConnect(G_wmpsock);
	}
	G_wmpsock = udpNewConnect(0, 3000);
	if (G_wmpsock < 1) {
		xyz_log_error("New UDP connect error");
		*state = WTP_STATE_SULK;
		return;
	}
//	apid = (uint8_t *)nsap_deid_get_byte();
        apid = (uint8_t *)devid;
        
	seqnum = wtpInfoGetSEQNUM();
	if (wmmClearInit(&reqmsg, apid, seqnum, WMM_CODE_DSCV_REQ)) {
		xyz_log_error("Message head INIT error");
		*state = WTP_STATE_SULK;
		return;
	}
	nsap_firm_ver_t *ver = (nsap_firm_ver_t *)nsap_firm_version();
	wmmPackBoard(&attr, ver->major, ver->minor, ver->patch, wtpInfoGetBoardName());
        
	if (wmmPutAttrBuff(&reqmsg, &attr)) {
		xyz_log_error("Put attribute error");
		*state = WTP_STATE_SULK;
		return;
	}
	//  添加ORGBRC
        
	do {
         
		if (nsap_orgbrc_test()) {
			xyz_log_warn("ORGBRC is invaild");
			break;
		}
            
		//  pack ORG
		xyz_str_t *id = nsap_orgbrc_get_org();
		if (!id || wmmPackOrg(&attr, id->data, id->len)) {
			xyz_log_warn("Pack ORG is failed");
			break;
		}
                xyz_log_debug("orgid is : %s",(char *)id->data);
		if (wmmPutAttrBuff(&reqmsg, &attr)) {
			xyz_log_error("Put attribute error");
			*state = WTP_STATE_SULK;
			return;
		}
		//  pack BRC
		id = nsap_orgbrc_get_brc();
		if (!id || wmmPackBrc(&attr, id->data, id->len)) {
			xyz_log_warn("Pack BRC is failed");
			break;
		}
		if (wmmPutAttrBuff(&reqmsg, &attr)) {
			xyz_log_error("Put attribute error");
			*state = WTP_STATE_SULK;
			return;
		}
	} while (0);
        
	if (udpSend(G_wmpsock, reqmsg.buff, reqmsg.offset, &G_wmpaddr) < 1) {
		xyz_log_error("sendto : %s", strerror(errno));
		*state = WTP_STATE_SULK;
		return;
	}
        
	memset(&repmsg, 0, sizeof(repmsg));
	len = udpRecv(G_wmpsock, repmsg.buff, WMM_BUFF_SIZE, &from);
	if (len < 1) {
		xyz_log_error("dscv udp recvfrom : %s", strerror(errno));
		*state = WTP_STATE_SULK;
		return;
	}
	if (from.sin_addr.s_addr != G_wmpaddr.sin_addr.s_addr) {
		xyz_log_error("Message source is error");
		*state = WTP_STATE_SULK;
		return;
	}
	repmsg.totlen = len;
	if (wmmPrepRecvd(&repmsg)) {
		xyz_log_error("Preprocess message error");
		*state = WTP_STATE_SULK;
		return;
	}
	if (repmsg.seqnum != seqnum) {
		xyz_log_error("Response message SEQ-NUM %d don't match %d", repmsg.seqnum, seqnum);
		*state = WTP_STATE_SULK;
		return;
	}
	if (WMM_CODE_DSCV_REP == repmsg.code) {
		*state = WTP_STATE_JOIN;
//		system("/sbin/ns_led.sh active &");
	} else {
		xyz_log_error("Response message code %d don't match %d", repmsg.code, WMM_CODE_DSCV_REP);
		*state = WTP_STATE_SULK;
	}
}


