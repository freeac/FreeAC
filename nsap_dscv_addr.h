/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */

#ifndef __NSAP_DSCV_ADDR_H__
#define __NSAP_DSCV_ADDR_H__

int nsap_dscv_addr_init(void);

const char *nsap_dscv_addr_get_addr(void);

int nsap_dscv_addr_get_off(void);

int nsap_dscv_addr_set(const char *addr, unsigned int off);

#endif /* __NSAP_DSCV_ADDR_H__ */
