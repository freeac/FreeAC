/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */

#ifndef __NSAP_80211_H__
#define __NSAP_80211_H__

#include <stdio.h>

#define NSAP_80211		1
#define NSAP_80211A		2
#define NSAP_80211B		3
#define NSAP_80211C		4
#define NSAP_80211D		5
#define NSAP_80211E		6
#define NSAP_80211F		7
#define NSAP_80211G		8
#define NSAP_80211H		9
#define NSAP_80211I		10
#define NSAP_80211J		11
#define NSAP_80211K		12
#define NSAP_80211L		13
#define NSAP_80211M		14
#define NSAP_80211N		15
#define NSAP_80211O		16
#define NSAP_80211P		17
#define NSAP_80211Q		18
#define NSAP_80211R		19
#define NSAP_80211S		20
#define NSAP_80211T		21
#define NSAP_80211U		22
#define NSAP_80211V		23
#define NSAP_80211AC		24
#define NSAP_80211AD		25
#define NSAP_80211AE		26

#define NSAP_80211_MAX	NSAP_80211AE





#endif /* __NSAP_80211_H__ */
