/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */

#ifndef __WTPINFO_H__
#define __WTPINFO_H__

#include "wmpctrl.h"


#define WTP_FIRMWARE_MAJOR_VERSION	1

#define WTP_FIRMWARE_MINOR_VERSION	0


//  /dev/mtdblock3
//  ./wtpid.cfg
//#define WTPID_PATH	"/dev/mtdblock3"
#define WTPID_MTD_NAME	"natshell"

//  /dev/mtdblock4
//  ./acaddr.cfg
//#define WTPACADDR_PATH	"/dev/mtdblock4"
#define WTP_ECHO_MAX	6

#define NFHOOK_METHOD_BRIDGE	1
#define NFHOOK_METHOD_ROUTE		2

#define WTP_STATE_INIT	0
#define WTP_STATE_DSCV	1
#define WTP_STATE_SULK	2
#define WTP_STATE_JOIN	3
#define WTP_STATE_IMAGE	4
#define WTP_STATE_RUN	5
#define WTP_STATE_WAIT	6
#define WTP_STATE_RECOV	7


int wtpInfoSetBoardName(const char *v, int len);


uint32_t wtpInfoSetIdle(uint32_t idle);
uint32_t wtpInfoGetIdle(void);

uint32_t wtpInfoSetEcho(uint32_t echo);
uint32_t wtpInfoGetEcho(void);

uint32_t wtpInfoSetAcct(uint32_t acct);
uint32_t wtpInfoGetAcct(void);

const char *wtpinfoGetACIPStr(void);
const char *wtpinfoSetACIPStr(const char *s);

struct wifi_cfg *wtpInfoGetWIFI(void);

struct prt_cfg *wtpInfoGetPortal(void);


///////////////////////////////////////////////////

int wtpInfoSendApid2Krn(void);
int wtpInfoSendOrgBrc2Krn(void);
int wtpInfoSendIdle2Krn(void);
int wtpInfoSendPrt2Krn(void);
int wtpInfoSendAcct2Krn(void);

int wtpInfoSendHookOpen2Krn(uint32_t method, const char *dev);
int wtpInfoSendHookClose2Krn(void);
int wtpInfoSendHookPause2Krn(void);
int wtpInfoSendHookRestart2Krn(void);

int wtpWiFiWirteFile(int wlanindex, int encrypt, const char *radio, int channel,
		int ssidsupp, const char *ssid, const char *passwd);

int wtpWiFiOpen(void);

int wtpWiFiClose(void);

int wtpWiFiCloseThe(int wlanindex);


#endif /* __WTPINFO_H__ */
