/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */

#ifndef __XYZ_DEF_H__
#define __XYZ_DEF_H__

#ifdef __KERNEL__

#include <linux/gfp.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/stddef.h>

#define xyz_calloc(_size_)	kcalloc(1, (_size_), GFP_KERNEL)

#define xyz_realloc(_ptr_, _size_)	krealloc((_ptr_), (_size_), GFP_KERNEL)

#define xyz_free(_p_)	\
		do {	\
			if ((_p_)) {	\
				kfree((_p_));	\
				(_p_) = (void *)0;	\
			}	\
		} while(0)

#define xyz_memcpy(_dst_, _src_, _len_)	memcpy((_dst_), (_src_), (_len_))
#define xyz_memcmp(_dst_, _src_, _len_)	memcmp((_dst_), (_src_), (_len_))

#else

#include <stdlib.h>
#include <string.h>
#include <stddef.h>

#define xyz_calloc(_size_)	calloc(1, (_size_))

#define xyz_realloc(_ptr_, _size_)	realloc((_ptr_), (_size_))

#define xyz_free(_p_)	\
	do {	\
		if (_p_) {	\
			free((_p_));	\
			(_p_) = (void *)0;	\
		}	\
	} while(0)

#define xyz_memcpy(_dst_, _src_, _len_)	memcpy((_dst_), (_src_), (_len_))
#define xyz_memcmp(_dst_, _src_, _len_)	memcmp((_dst_), (_src_), (_len_))

#endif


#define xyz_kb(_n_)	((_n_) << 10)
#define xyz_mb(_n_)	((_n_) << 20)
#define xyz_gb(_n_)	((_n_) << 30)

#endif /* __XYZ_DEF_H__ */
