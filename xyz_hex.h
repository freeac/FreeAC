/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */

#ifndef __XYZ_HEX_H__
#define __XYZ_HEX_H__

#include "xyz_int.h"

static const unsigned char S_xyz_hex[] = "0123456789abcdef";
static const unsigned char S_xyz_HEX[] = "0123456789ABCDEF";

static inline void xyz_hex_u8_upper(xyz_u8_t n, xyz_u8_t *buf)
{
	buf[0] = S_xyz_HEX[(n >> 4) & 0xF];
	buf[1] = S_xyz_HEX[n & 0xF];
}

static inline void xyz_hex_u8_lower(xyz_u8_t n, xyz_u8_t *buf)
{
	buf[0] = S_xyz_hex[(n >> 4) & 0xF];
	buf[1] = S_xyz_hex[n & 0xF];
}

static inline void xyz_hex_u16_upper(xyz_u16_t n, xyz_u8_t *buf)
{
	buf[0] = S_xyz_HEX[(n >> 12) & 0xF];
	buf[1] = S_xyz_HEX[(n >> 8) & 0xF];
	buf[2] = S_xyz_HEX[(n >> 4) & 0xF];
	buf[3] = S_xyz_HEX[n & 0xF];
}

static inline void xyz_hex_u16_lower(xyz_u16_t n, xyz_u8_t *buf)
{
	buf[0] = S_xyz_hex[(n >> 12) & 0xF];
	buf[1] = S_xyz_hex[(n >> 8) & 0xF];
	buf[2] = S_xyz_hex[(n >> 4) & 0xF];
	buf[3] = S_xyz_hex[n & 0xF];
}

static inline void xyz_hex_u32_upper(xyz_u32_t n, xyz_u8_t *buf)
{
	buf[0] = S_xyz_HEX[(n >> 28) & 0xF];
	buf[1] = S_xyz_HEX[(n >> 24) & 0xF];
	buf[2] = S_xyz_HEX[(n >> 20) & 0xF];
	buf[3] = S_xyz_HEX[(n >> 16) & 0xF];
	buf[4] = S_xyz_HEX[(n >> 12) & 0xF];
	buf[5] = S_xyz_HEX[(n >> 8) & 0xF];
	buf[6] = S_xyz_HEX[(n >> 4) & 0xF];
	buf[7] = S_xyz_HEX[n & 0xF];
}

static inline void xyz_hex_u32_lower(xyz_u32_t n, xyz_u8_t *buf)
{
	buf[0] = S_xyz_hex[(n >> 28) & 0xF];
	buf[1] = S_xyz_hex[(n >> 24) & 0xF];
	buf[2] = S_xyz_hex[(n >> 20) & 0xF];
	buf[3] = S_xyz_hex[(n >> 16) & 0xF];
	buf[4] = S_xyz_hex[(n >> 12) & 0xF];
	buf[5] = S_xyz_hex[(n >> 8) & 0xF];
	buf[6] = S_xyz_hex[(n >> 4) & 0xF];
	buf[7] = S_xyz_hex[n & 0xF];
}

int xyz_hex_byte_upper(const xyz_u8_t *b, xyz_sli_t len, xyz_u8_t *buf, xyz_sli_t size);

int xyz_hex_byte_lower(const xyz_u8_t *b, xyz_sli_t len, xyz_u8_t *buf, xyz_sli_t size);

xyz_u8_t xyz_hex_digit(xyz_u8_t ch);

xyz_u64_t xyz_hex_str_int(const xyz_u8_t *s, xyz_sli_t len);

static inline xyz_sli_t xyz_hex_str_byte_size(xyz_sli_t len)
{
	return ((len % 2) ? ((len + 1) / 2) : (len / 2));
}

int xyz_hex_str_byte(const xyz_u8_t *s, xyz_sli_t len, xyz_u8_t *buf, xyz_sli_t size);

int xyz_hex_MAC_str_byte(const xyz_u8_t *s, xyz_sli_t len, xyz_u8_t *buf, xyz_sli_t size);

int xyz_hex_MAC_byte_upper(const xyz_u8_t *in, xyz_sli_t len, xyz_u8_t *out, xyz_sli_t size);

int xyz_hex_MAC_byte_lower(const xyz_u8_t *in, xyz_sli_t len, xyz_u8_t *out, xyz_sli_t size);


#endif /* __XYZ_HEX_H__ */
