/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */

#ifndef __WMM_USER_H__
#define __WMM_USER_H__

#include <stdint.h>
#include "buff.h"
#include "wmp.h"

struct user_info {
	uint32_t sn;
	uint32_t ip;
	uint8_t mac[6];
	uint16_t wlanindex;
	uint32_t errcode;
};

struct user_quota {
	uint32_t sn;
	uint32_t ip;
	uint8_t mac[6];
	uint16_t wlanindex;
	uint32_t session;
	uint32_t inspeed;
	uint32_t outspeed;
};

struct user_stat {
	uint32_t ip;
	uint8_t mac[6];
	uint16_t wlanindex;
	uint32_t online;
	uint32_t ingiga;
	uint32_t in;
	uint32_t outgiga;
	uint32_t out;
};

struct user_auth {
	uint8_t nlen;
	char name[128];
	uint8_t plen;
	char pass[128];
};


int wmmPackUserInfo(BUFF *b, struct user_info *v);
int wmmParseUserInfo(BUFF *b, struct user_info *v);

int wmmPackUserAuth(BUFF *b, const char *name, int nlen, const char *pass, int plen);
int wmmParseUserAuth(BUFF *b, struct user_auth *v);

int wmmPackUserQuota(BUFF *b, struct user_quota *v);
int wmmParseUserQuota(BUFF *b, struct user_quota *v);

int wmmPackUserStat(BUFF *b, struct user_stat *v);
int wmmParseUserStat(BUFF *b, struct user_stat *v);

#endif /* __WMM_USER_H__ */
