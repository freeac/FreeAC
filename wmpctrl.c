/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *
 *   版权所有  2014-2015 成都星锐蓝海网络科技有限公司
 *   商业许可请联系  +86-18682011860    QQ:66442834
 *   
 */


#include <stdio.h>
#include "xyz_log.h"
#include "buff.h"
#include "wmpctrl.h"

int wmmPackSecKey(BUFF *b, struct sec_key *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffInit(b, WMM_ATTR_SECKEY);
	buffPutU32(b, v->keylen);
	buffPutStr(b, v->rawkey, strlen(v->rawkey));
	return 0;
}

int wmmParseSecKey(BUFF *b, struct sec_key *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffGetU32(b, &(v->keylen));
	buffGetStr(b, v->rawkey, b->len - sizeof(uint32_t));
	return ((b->offset == b->len) ? 0 : -1);
}

int wmmPackBoard(BUFF *b, uint8_t major, uint8_t minor, uint16_t patch, const char *name)
{
	if (!b || !name) {
		xyz_log_error("Args error");
		return -1;
	}
	buffInit(b, WMM_ATTR_BOARD);
	buffPutU8(b, major);
	buffPutU8(b, minor);
	buffPutU16(b, patch);
	buffPutStr(b, name, strlen(name));
	return 0;
}

int wmmPackOrg(BUFF *b, const char *org, int len)
{
	if (!b || !org) {
		xyz_log_error("Args error");
		return -1;
	}
	buffInit(b, WMM_ATTR_ORG);
	buffPutStr(b, org, len);
	return 0;
}

int wmmParseOrg(BUFF *b, char *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffGetStr(b, v, b->len);
	return ((b->offset == b->len) ? 0 : -1);
}

int wmmPackBrc(BUFF *b, const char *brc, int len)
{
	if (!b || !brc) {
		xyz_log_error("Args error");
		return -1;
	}
	buffInit(b, WMM_ATTR_BRC);
	buffPutStr(b, brc, len);
	return 0;
}

int wmmParseBrc(BUFF *b, char *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffGetStr(b, v, b->len);
	return ((b->offset == b->len) ? 0 : -1);
}

int wmmParseBoard(BUFF *b, struct board *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffGetU8(b, &(v->major));
	buffGetU8(b, &(v->minor));
	buffGetU16(b, &(v->patch));
	buffGetStr(b, v->hardware, b->len - sizeof(uint32_t));
	return ((b->offset == b->len) ? 0 : -1);
}


int wmmParseImageIdent(BUFF *b, struct img_ident *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffGetByte(b, v->sessid, b->len);
	return 0;
}


int wmmPackResCode(BUFF *b, uint32_t code)
{
	if (!b) {
		xyz_log_error("Args error");
		return -1;
	}
	buffInit(b, WMM_ATTR_RESCODE);
	buffPutU32(b, code);
	return 0;
}

int wmmParseResCode(BUFF *b, uint32_t *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffGetU32(b, v);
	return ((b->offset == b->len) ? 0 : -1);
}

int wmmPackEcho(BUFF *b, uint32_t v)
{
	if (!b) {
		xyz_log_error("Args error");
		return -1;
	}
	buffInit(b, WMM_ATTR_ECHO);
	buffPutU32(b, v);
	return 0;
}

int wmmParseEcho(BUFF *b, uint32_t *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffGetU32(b, v);
	return ((b->offset == b->len) ? 0 : -1);
}

int wmmPackIdle(BUFF *b, uint32_t v)
{
	if (!b) {
		xyz_log_error("Args error");
		return -1;
	}
	buffInit(b, WMM_ATTR_IDLE);
	buffPutU32(b, v);
	return 0;
}

int wmmParseIdle(BUFF *b, uint32_t *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffGetU32(b, v);
	return ((b->offset == b->len) ? 0 : -1);
}


int wmmParseWIFI(BUFF *b, struct wifi_cfg *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffGetU8(b, &(v->portal));
	buffGetU8(b, &(v->radiotype));
	buffGetU8(b, &(v->channel));
	buffGetU8(b, &(v->encrypt));

	buffGetU8(b, &(v->arithmetic));
	buffGetU8(b, &(v->acctenable));
	buffGetU8(b, &(v->RSSI));
	buffGetU8(b, &(v->maxassoc));

	buffGetU8(b, &(v->ssidsupp));
	buffGetU8(b, &(v->wlanindex));
	buffGetU16(b, &(v->power));


	buffGetU8(b, &(v->ssidlen));
	buffGetStr(b, v->ssid, v->ssidlen);

	buffGetU8(b, &(v->keylen));
	buffGetStr(b, v->key, v->keylen);

	return ((b->offset == b->len) ? 0 : -1);
}

int wmmParsePortal(BUFF *b, struct prt_cfg *v)
{
	if (!b || !v) {
		xyz_log_error("Args error");
		return -1;
	}
	buffGetIPAddr(b, &(v->srvip));
	buffGetIPAddr(b, &(v->nasip));
	buffGetU16(b, &(v->proto));
	buffGetU16(b, &(v->urllen));
	buffGetStr(b, v->url, v->urllen);
	return ((b->offset == b->len) ? 0 : -1);
}
